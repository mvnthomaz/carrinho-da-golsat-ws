package br.com.golsat.carrinhodagolsatws.data

import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.NaturalIdCache
import java.util.*
import javax.persistence.*


@Entity(name = "Ordered")
@Table(name = "ordered")
@NaturalIdCache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
class Ordered {
        @Id
        @GeneratedValue
        var id: Int = 0

        @OneToOne(cascade = [CascadeType.PERSIST])
        @JoinColumn(name = "employer_id", nullable = false, insertable = false, updatable = false)
        val employer: Employer? = null

        @OneToMany(
                mappedBy = "ordered",
                cascade = [CascadeType.ALL],
                orphanRemoval = true
        )
        var items: List<Item>? = null

        var createdAt: Date? = null
}