package br.com.golsat.carrinhodagolsatws.data

import javax.persistence.*


@Entity(name = "Product")
@Table(name = "product")
data class Product (
        @Id
        @GeneratedValue
        val id: Int = 0,

        val name: String? = null,

        val price: Float = 0F,

        val available: Boolean = false

//        @OneToMany(mappedBy = "product", cascade = [CascadeType.ALL], orphanRemoval = true)
//        val items: List<Item>? = null
)