package br.com.golsat.carrinhodagolsatws.data

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class IdItem : Serializable {
    @Column(name = "ordered_id")
    var ordered: Long = 0
    @Column(name = "product_id")
    var product: Long = 0
}