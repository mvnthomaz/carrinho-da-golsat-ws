package br.com.golsat.carrinhodagolsatws.data

class Person {

    var altura
    var peso    = 75

    val imc
        get() = peso / Math.pow(altura, 2.0)
}