package br.com.golsat.carrinhodagolsatws.data

import javax.persistence.*

@Entity
data class Employer (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Int = 0,

        val name: String? = null,

        @OneToOne(cascade = [CascadeType.PERSIST])
        @JoinColumn(name = "department_id")
        val department: Department? = null,

        val available: Boolean
){
        private constructor() : this(0, "", null, false)

        companion object {
                val STUB = Employer()
        }
}