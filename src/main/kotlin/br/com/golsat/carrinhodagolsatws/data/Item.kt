package br.com.golsat.carrinhodagolsatws.data

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity(name = "Item")
@Table(name = "item")
class Item {
    @JsonIgnore
    @EmbeddedId
    var id: IdItem? = null

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("ordered")
    var ordered: Ordered? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("product")
    var product: Product? = null

    var quantity: Int = 0
    var price: Float = 0F
}
