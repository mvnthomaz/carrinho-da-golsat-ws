package br.com.golsat.carrinhodagolsatws

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CarrinhodagolsatWsApplication

fun main(args: Array<String>) {
	runApplication<CarrinhodagolsatWsApplication>(*args)
}
