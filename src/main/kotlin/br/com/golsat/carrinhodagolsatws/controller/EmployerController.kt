package br.com.golsat.carrinhodagolsatws.controller

import br.com.golsat.carrinhodagolsatws.common.FilterEmployerSpecification
import br.com.golsat.carrinhodagolsatws.data.Department
import br.com.golsat.carrinhodagolsatws.data.Employer
import br.com.golsat.carrinhodagolsatws.repository.DepartmentRepository
import br.com.golsat.carrinhodagolsatws.repository.EmployerRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class EmployerController (private val employerRepository: EmployerRepository,
                          private val departmentRepository: DepartmentRepository) {

    @GetMapping("/employers")
    fun getAll(): List<Employer> =
            employerRepository.findAll(FilterEmployerSpecification())

    @PostMapping("/employer")
    fun createNew(@Valid @RequestBody employer: Employer): Employer =
            employerRepository.save(employer)

    @GetMapping("/employer/{id}")
    fun getById(@PathVariable(value = "id") employerId: Int): ResponseEntity<Employer> {
        return employerRepository.findById(employerId).map { employer ->
            ResponseEntity.ok(employer)
        }.orElse(ResponseEntity.notFound().build())
    }

    @PutMapping("/employer/{id}")
    fun updateById(@PathVariable(value = "id") employerId: Int,
                   @Valid @RequestBody newEmployer: Employer): ResponseEntity<Employer> {

        return employerRepository.findById(employerId).map { existingEmployer ->
            val updatedEmployer: Employer = existingEmployer
                    .copy(name = newEmployer.name)

            ResponseEntity.ok().body(employerRepository.save(updatedEmployer))
        }.orElse(ResponseEntity.notFound().build())

    }

    @DeleteMapping("/employer/{id}")
    fun deleteById(@PathVariable(value = "id") employerId: Int): ResponseEntity<Void> {

        return employerRepository.findById(employerId).map { employer  ->
            employerRepository.delete(employer)
            ResponseEntity<Void>(HttpStatus.OK)
        }.orElse(ResponseEntity.notFound().build())

    }

    @GetMapping("/departments")
    fun getAllDepartments(): List<Department> =
            departmentRepository.findAll()
}