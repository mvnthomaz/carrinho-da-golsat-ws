package br.com.golsat.carrinhodagolsatws.controller

import br.com.golsat.carrinhodagolsatws.data.Product
import br.com.golsat.carrinhodagolsatws.repository.ProductRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class ProductController (private val productRepository: ProductRepository) {

    @GetMapping("/products")
    fun getAll(): List<Product> =
            productRepository.findAll()

    @PostMapping("/product")
    fun createNew(@Valid @RequestBody product: Product): Product =
            productRepository.save(product)

    @PutMapping("/product/{id}")
    fun updateById(@PathVariable(value = "id") productId: Long,
                          @Valid @RequestBody newProduct: Product): ResponseEntity<Product> {

        return productRepository.findById(productId).map { existingProduct ->
            val updatedProduct: Product = existingProduct
                    .copy(name = newProduct.name)

            ResponseEntity.ok().body(productRepository.save(updatedProduct))
        }.orElse(ResponseEntity.notFound().build())

    }

    @DeleteMapping("/product/{id}")
    fun deleteById(@PathVariable(value = "id") productId: Long): ResponseEntity<Void> {

        return productRepository.findById(productId).map { article  ->
            productRepository.delete(article)
            ResponseEntity<Void>(HttpStatus.OK)
        }.orElse(ResponseEntity.notFound().build())

    }
}