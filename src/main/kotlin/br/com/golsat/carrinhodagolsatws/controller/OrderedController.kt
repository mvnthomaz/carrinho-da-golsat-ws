package br.com.golsat.carrinhodagolsatws.controller

import br.com.golsat.carrinhodagolsatws.common.SearchCriteriaYearMonth
import br.com.golsat.carrinhodagolsatws.common.FilterOrderedSpecification
import br.com.golsat.carrinhodagolsatws.data.Ordered
import br.com.golsat.carrinhodagolsatws.repository.OrderedRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class OrderedController (private val orderedRepository: OrderedRepository) {

    @Autowired
    private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate? = null

    @GetMapping("/orders/{year}/{month}")
    fun getAll(@PathVariable(value="year") year: Int, @PathVariable(value="month") month: Int): List<Ordered> {
        val spec = FilterOrderedSpecification(SearchCriteriaYearMonth("=", year, month))
        return orderedRepository.findAll(spec)
    }

    @GetMapping("/orders-grouped")
    fun findAllGroupBy() : Specification<Ordered> {
        var a: Specification<Ordered> = Specification { root, query, builder ->
            query.groupBy(root.get<Ordered>("employer")).having(
                    builder.greaterThan(builder.function("COUNT", Int::class.java, root.get<Ordered>("createdAt")), 0)
            ).groupRestriction
        }
        return a
    }

    @PostMapping("/order", consumes = ["application/json"], produces = ["application/json"])
    fun createNew(@Valid @RequestBody ordered: Ordered): Ordered {
        var query = "insert into ordered (employer_id, created_at) values (:employer_id, :created_at)"

        val holder = GeneratedKeyHolder()
        var parameters = MapSqlParameterSource()
                .addValue("employer_id", ordered.employer!!.id)
                .addValue("created_at", Date())
        namedParameterJdbcTemplate!!.update(query, parameters, holder)

        val idOrdered = holder.key!!.toInt()

        query = "insert into item (ordered_id, product_id, quantity, price) values " +
                "(:ordered_id, :product_id, :quantity, :price)"

        ordered.items!!.forEach { item ->
            parameters = MapSqlParameterSource()
                    .addValue("ordered_id", idOrdered)
                    .addValue("product_id", item.product!!.id)
                    .addValue("quantity", item.quantity)
                    .addValue("price", item.product!!.price)

            namedParameterJdbcTemplate.update(query, parameters, holder)
        }
        ordered.items = null
        ordered.id = idOrdered
        return ordered
    }

    @DeleteMapping("/order/{id}")
    fun deleteById(@PathVariable(value = "id") id: Int): ResponseEntity<Void> {

        return orderedRepository.findById(id).map { order  ->
            orderedRepository.delete(order)
            ResponseEntity<Void>(HttpStatus.OK)
        }.orElse(ResponseEntity.notFound().build())
    }
}