package br.com.golsat.carrinhodagolsatws.repository

import br.com.golsat.carrinhodagolsatws.data.Employer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface EmployerRepository : JpaRepository<Employer, Int>, JpaSpecificationExecutor<Employer>