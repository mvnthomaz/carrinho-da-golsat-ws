package br.com.golsat.carrinhodagolsatws.repository

import br.com.golsat.carrinhodagolsatws.data.Department
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DepartmentRepository : JpaRepository<Department, Long>