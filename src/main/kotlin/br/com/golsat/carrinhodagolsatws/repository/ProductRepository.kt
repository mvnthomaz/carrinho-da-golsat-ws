package br.com.golsat.carrinhodagolsatws.repository

import br.com.golsat.carrinhodagolsatws.data.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : JpaRepository<Product, Long>