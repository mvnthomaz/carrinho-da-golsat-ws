package br.com.golsat.carrinhodagolsatws.repository

import br.com.golsat.carrinhodagolsatws.data.Ordered
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface OrderedRepository : JpaRepository<Ordered, Int>, JpaSpecificationExecutor<Ordered>