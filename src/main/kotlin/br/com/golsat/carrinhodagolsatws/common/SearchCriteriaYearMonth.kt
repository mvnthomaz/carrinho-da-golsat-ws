package br.com.golsat.carrinhodagolsatws.common

class SearchCriteriaYearMonth(operation: String, year: Any, month: Any) {

    var operation: String? = operation
    var year: Any? = year
    var month: Any? = month
}