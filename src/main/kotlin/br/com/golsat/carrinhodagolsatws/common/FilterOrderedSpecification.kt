package br.com.golsat.carrinhodagolsatws.common

import br.com.golsat.carrinhodagolsatws.data.Ordered
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root


class FilterOrderedSpecification(searchCriteriaYearMonth: SearchCriteriaYearMonth) : Specification<Ordered> {

    private var criteriaYearMonth: SearchCriteriaYearMonth? = searchCriteriaYearMonth

    override fun toPredicate(root: Root<Ordered>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when {
            criteriaYearMonth!!.operation.equals("=") -> {
                builder.and(
                        builder.equal(builder.function("YEAR", Int::class.java, root.get<Ordered>("createdAt")), criteriaYearMonth!!.year),
                        builder.equal(builder.function("MONTH", Int::class.java, root.get<Ordered>("createdAt")), criteriaYearMonth!!.month)

                )
            } else -> null
        }
    }

}