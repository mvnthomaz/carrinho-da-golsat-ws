package br.com.golsat.carrinhodagolsatws.common

import br.com.golsat.carrinhodagolsatws.data.Employer
import br.com.golsat.carrinhodagolsatws.data.Ordered
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root


class FilterEmployerSpecification : Specification<Employer> {

    override fun toPredicate(root: Root<Employer>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return builder.equal(root.get<Employer>("available"), true)
    }
}