package br.com.golsat.carrinhodagolsatws.common

class SearchCriteriaEmployer(operation: String, year: Any, month: Any) {

    var operation: String? = operation
    var year: Any? = year
    var month: Any? = month
}